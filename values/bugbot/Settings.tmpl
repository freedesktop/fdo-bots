debug: false
listen_address: "::"
port: 8080

# this is a yaml anchors definition of a rule, that can be reused multiple
# times in the webhooks.
.is_issue: &is_issue
  # the hook is of kind "issue"
  equals:
    key: "object_kind"
    value: "issue"

.is_confidential_issue: &is_confidential_issue
  # the hook is of kind "confidential issue"
  equals:
    key: "event_type"
    value: "confidential_issue"

.is_push: &is_push
  # the hook is of kind "push"
  equals:
    key: "object_kind"
    value: "push"

.export_url: &export_url
  # set OBJECT_URL to the object_attributes.url value
  follow:
    key: "object_attributes"
    op:
      export:
        key: "url"
        name: "OBJECT_URL"

.is_not_bugbot: &is_not_bugbot
  # the hook was not triggered by the bugbot
  follow:
    key: "user"
    op:
      not:
        equals:
          key: "username"
          value: "bugbot"

.is_merge_request: &is_merge_request
  # the hook is of kind "merge_request"
  equals:
    key: "event_type"
    value: "merge_request"

.is_open_action: &is_open_action
  # the object is an 'open' action
  follow:
    key: "object_attributes"
    op:
      equals:
        key: "action"
        value: "open"

.is_update_action: &is_update_action
  # the object is an 'update' action
  follow:
    key: "object_attributes"
    op:
      equals:
        key: "action"
        value: "update"


.is_pipeline: &is_pipeline
  # hook is of kind "pipeline"
  equals:
    key: "object_kind"
    value: "pipeline"

.is_scheduled_pipeline: &is_scheduled_pipeline
  # hook is of kind "pipeline" and
  and:
    - *is_pipeline
    - follow:
        key: "object_attributes"
        op:
          equals:
            key: "source"
            value: "schedule"

.is_successful_pipeline: &is_successful_pipeline
  # hook is of kind "pipeline" and status is success
  and:
    - *is_pipeline
    - follow:
        key: "object_attributes"
        op:
          equals:
            key: "status"
            value: "success"


.has_bugbot_label: &has_bugbot_label
  # there is a change in the labels, and "bugbot::" is now present
  follow:
    key: "changes"
    op:
      follow:
        key: "labels"
        op:
          any:
            key: "current"
            op:
              substring:
                key: "title"
                needle: "bugbot::"

.reload_command: &reload_command
  - "bash"
  - "-c"
  - "cd /shared && curl -LO -f --retry 4 --retry-delay 60 {{ SCRIPT_URL }} && bash /shared/prep.sh"

.reload_env: &reload_env
  - "SCRIPT_URL"
  - "NAME"
  - "SETTINGS"
  - "PROJECT_REPO"
  - "SECRETS_FILE"


# define a global workload for reloading the hook configuration
.autoreload_config_on_push: &autoreload_config_on_push
  name: update when push
  description: |
    update all config when there is a push, and reload
    hookiedookie
  rule:
    *is_push
  run:
    *reload_command
  env:
    *reload_env

.autoreload_config_on_fdo_bots_push: &autoreload_config_on_fdo_bots_push
  name: update when push on fdo_bots
  description: |
    update all config when there is a push touching selected files,
    and reload hookiedookie
  run:
    *reload_command
  env:
    *reload_env
  rule:
    and:
      - *is_push
      - any:
          key: commits
          op:
            or:
              - any:
                  key: modified
                  op:
                    substring:
                      needle: helm-gitlab-secrets
              - any:
                  key: modified
                  op:
                    substring:
                      needle: values/hookiedookie
              - any:
                  key: added
                  op:
                    substring:
                      needle: values/{{ PROJECT_REPO }}
              - any:
                  key: modified
                  op:
                    substring:
                      needle: values/{{ PROJECT_REPO }}
              - any:
                  key: removed
                  op:
                    substring:
                      needle: values/{{ PROJECT_REPO }}

.process_webhook_request: &process_webhook_request
  name: process webhook request
  description: |
    When a webhook request is filed, call into bugbot to
    process that request.
  rule:
    and:
      - *is_issue
      - *is_not_bugbot
      - *export_url
      - follow:
          key: "object_attributes"
          op:
            substring:
              key: "title"
              needle: "bugbot webhook request:"
  run:
    - "bash"
    - "-c"
    - "source /shared/venv/bugbot/bin/activate && bugbot --token-env GITLAB_TOKEN process-webhook-request $OBJECT_URL"
  env:
    - "GITLAB_TOKEN"


webhooks:
  - path: "freedesktop/fdo-bots"
    token: "{{ bugbot.fdo_bots.token}}"

    workloads:
      - *autoreload_config_on_fdo_bots_push
      - *process_webhook_request

  - path: "freedesktop/damspam"
    token: "{{ bugbot.fdo_damspam.token}}"

    workloads:
      - *autoreload_config_on_push

{% for webhook in bugbot.webhooks %}
  - path: "{{ webhook.path }}"
    token: "{{ webhook.token }}"

    workloads:
      - name: run bugbot gitlab-triage
        description: |
          A webhook that get called when there is a label starting with 'bugbot::' added to an issue:
          - it delays for 30s (to be able to undo accidental labels)
          - it fetches the .gitlab-triage.yml
          - it runs gitlab-triage with that policy file as the bugbot user

        rule:
          and:
            - *is_not_bugbot
            - or:
              - *is_issue
              - *is_merge_request
            - *export_url
            - *has_bugbot_label
            - follow:
                key: "project"
                op:
                  equals:
                    key: "path_with_namespace"
                    value: "{{ webhook.path }}"

        run:
          - "bash"
          - "-c"
          - "source /shared/venv/bugbot/bin/activate && bugbot --token-env GITLAB_TOKEN gitlab-triage --delay 30 $OBJECT_URL"
           

        # gitlab-triage runs with a 30s delay, so we must run this in the background
        background: true

        env:
          - "GITLAB_TOKEN"
{% endfor %}
