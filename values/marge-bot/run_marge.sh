#!/bin/bash

set -ex

HOOKS_DIR_ARG=""
if [[ -n "${MARGE_HOOKS_PROJECT}" ]]; then
    MARGE_HOOKS_PROJECT_DIR="/shared/${MARGE_HOOKS_PROJECT}"

    if [[ ! -d "${MARGE_HOOKS_PROJECT_DIR}" ]]; then
        mkdir -p "${MARGE_HOOKS_PROJECT_DIR}"
        pushd "${MARGE_HOOKS_PROJECT_DIR}"
        git init
        git config core.sparseCheckout true
        echo ".marge/hooks" > .git/info/sparse-checkout
        git remote add origin "https://gitlab.freedesktop.org/${MARGE_HOOKS_PROJECT}.git"
        DEFAULT_BRANCH=$(git remote show origin | sed -n '/HEAD branch/s/.*: //p')
        git config init.defaultBranch "${DEFAULT_BRANCH}"
        git config remote.origin.fetch "+refs/heads/${DEFAULT_BRANCH}:refs/remotes/origin/${DEFAULT_BRANCH}"
        git fetch origin "${DEFAULT_BRANCH}" --depth=1
        git checkout "${DEFAULT_BRANCH}"
        popd
    else
        pushd "${MARGE_HOOKS_PROJECT_DIR}"
        DEFAULT_BRANCH=$(git remote show origin | sed -n '/HEAD branch/s/.*: //p')
        git fetch origin "${DEFAULT_BRANCH}" --depth=1
        git checkout "${DEFAULT_BRANCH}"
        popd
    fi

    # subsequently expand this as two separate values, don't double quote
    if [[ -d  "${MARGE_HOOKS_PROJECT_DIR}/.marge/hooks" ]]; then
         HOOKS_DIR_ARG="--hooks-directory ${MARGE_HOOKS_PROJECT_DIR}/.marge/hooks"
    fi
fi

marge --gitlab-url=https://gitlab.freedesktop.org \
      --impersonate-approvers \
      --ci-timeout ${MARGE_CI_TIMEOUT} \
      --git-timeout 5m \
      --merge-order assigned_at \
      --project-regexp "${MARGE_PROJECT_REGEX}" \
      ${HOOKS_DIR_ARG} \
      ${MARGE_EXTRA_ARGS}
